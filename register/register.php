<?php
// Страница регистрации нового пользователя

// Соединямся с БД
$host = 'localhost';
$user = 'root';
$passwd = '';
$dbname = 'autoshop_db';

$link = mysqli_connect($host, $user, $passwd);
mysqli_select_db($link, $dbname);

if (isset($_POST['submit'])) {
    $err = [];

    // проверям логин
    if (!preg_match("/^[a-zA-Z0-9]+$/", $_POST['login'])) {
        $err[] = "Логин может состоять только из букв английского алфавита и цифр";
    }

    if (strlen($_POST['login']) < 3 or strlen($_POST['login']) > 30) {
        $err[] = "Логин должен быть не меньше 3-х символов и не больше 30";
    }

    // проверяем, не сущестует ли пользователя с таким именем
    $query = mysqli_query($link, "SELECT user_id FROM users WHERE user_login = '" . mysqli_real_escape_string($link, $_POST['login']) . "'");

    if (mysqli_num_rows($query) > 0) {
        $err[] = "Пользователь с таким логином уже существует в базе данных";
    }

    // Если нет ошибок, то добавляем в БД нового пользователя
    if (count($err) == 0) {

        $login = $_POST['login'];

        // Убираем лишние пробелы и делаем двойное хеширование
        $password = md5(md5(trim($_POST['password'])));

        mysqli_query($link, "INSERT INTO users SET user_login='" . $login . "', user_password='" . $password . "'");
        header("Location: login.php");
        exit();
    } else {
        print "<b>При регистрации произошли следующие ошибки:</b><br>";
        foreach ($err as $error) {
            print "<p class='error'>". $error . "</p>>" . "<br>";
        }
    }
}

mysqli_close($link)

?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Регистрация</title>
    <link rel="stylesheet" type="text/css" href="register-style.css" />
</head>
<body>
<div class="container">
    <section id="content">
        <form method="POST">
            <h1>Зарегистрироваться</h1>
            <div>
                <input type="text" placeholder="Username" name="login" required/>
            </div>
            <div>
                <input type="password" placeholder="Password" name="password" required/>
            </div>
            <div>
                <input type="submit" value="Register" name="submit"/>
                <a href="login.php">Авторизация</a>
            </div>
        </form><!-- form -->

    </section><!-- content -->
</div><!-- container -->
</body>
</html>