<?php

// Страница авторизации

$url = 'http://localhost:63342/wamp64/www/';

// Функция для генерации случайной строки
function generateCode($length = 6)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0, $clen)];
    }
    return $code;
}

// Соединямся с БД
$host = 'localhost';
$user = 'root';
$passwd = '';
$dbname = 'autoshop_db';

$link = mysqli_connect($host, $user, $passwd);
mysqli_select_db($link, $dbname);

if (isset($_POST['submit'])) {
    // Вытаскиваем из БД запись, у которой логин равняеться введенному
    $query = mysqli_query($link, "SELECT user_id, user_password FROM users WHERE user_login='" . mysqli_real_escape_string($link, $_POST['login']) . "' LIMIT 1");
    $data = mysqli_fetch_assoc($query);

    // Сравниваем пароли
    if ($data['user_password'] === md5(md5($_POST['password']))) {
        // Генерируем случайное число и хешируем его
        $hash = md5(generateCode(10));

        // Записываем в БД новый хеш авторизации и IP
        mysqli_query($link, "UPDATE users SET user_hash='" . $hash . "' " .  " WHERE user_id='" . $data['user_id'] . "'");

        // Ставим куки
        setcookie("id", $data['user_id'], time() + 60 * 60 * 24 * 30, "/");
        setcookie("hash", $hash, time() + 60 * 60 * 24 * 30, "/", null, null, true);

        // Переадресовываем браузер на страницу проверки нашего скрипта
        header("Location: " . $url . "layout.php");
        exit();
    } else {
        print "Вы ввели неправильный логин/пароль";
    }
}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Вход</title>
    <link rel="stylesheet" type="text/css" href="register-style.css" />
</head>
<body>
<div class="container">
    <section id="content">
        <form method="POST">
            <h1>Войти</h1>
            <div>
                <input type="text" placeholder="Username" name="login" required/>
            </div>
            <div>
                <input type="password" placeholder="Password" name="password" required/>
            </div>
            <div>
                <input type="submit" value="Log in" name="submit"/>
                <a href="register.php">Регистрация</a>
            </div>
        </form><!-- form -->

    </section><!-- content -->
</div><!-- container -->
</body>
</html>
