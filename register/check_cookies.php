<?php

function check_cookies()
{
    $url = 'http://localhost:63342/wamp64/www/';

    $host = 'localhost';
    $user = 'root';
    $passwd = '';
    $dbname = 'autoshop_db';

    $link = mysqli_connect($host, $user, $passwd);
    mysqli_select_db($link, $dbname);

    if (isset($_COOKIE['id']) and isset($_COOKIE['hash'])) {
        $select_string = "SELECT * FROM users WHERE user_id = '" . intval($_COOKIE['id']) . "' LIMIT 1";
        $query = mysqli_query($link, $select_string);
        $user_data = mysqli_fetch_assoc($query);

        if ($user_data['user_hash'] === $_COOKIE['hash']) {
            return true;
        } else {
            setcookie("id", "", time() - 3600 * 24 * 30 * 12, "/");
            setcookie("hash", "", time() - 3600 * 24 * 30 * 12, "/", null, null, true);
            return false;
        }
    } else {
        $message = 'Включите cookies';
        echo "<script type='text/javascript'>alert('$message');</script>";
        return false;
    }
}
