<!DOCTYPE html>

<?php include_once "register/check_cookies.php" ?>

<html lang="ru">
<head>
    <title><?= $title ?></title>
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
<header>
    <div id="logo"></div>
    <div id="nav">
        <div class="catalog-href">
            <a>Каталог</a>
        </div>
        <div class="garage-href">
            <a>Мой гараж</a>
        </div>
        <div class="register-href">
            <?php
            if (check_cookies()) {
                echo "<a href='register/exit.php'>Выйти</a>";
            } else {
                echo "<a href='register/login.php'>Авторизация</a>";
            }
            ?>
        </div>
    </div>
    <div id="blank-header-space"></div>
</header>

<div class="main-content">
    <main class="content">
        <?= $content ?>
    </main>
</div>

<div id="page-left"></div>
<div id="page-right"></div>

<footer>
    <div class="blank-footer-left"></div>
    <div class="footer"></div>
    <div class="blank-footer-right"></div>
</footer>


</html>
