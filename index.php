<?php
include_once "register/check_cookies.php";
include_once "rendering.php";

$url = 'http://localhost:63342/wamp64/www/';

if (!check_cookies()) {
    header("Location: " . $url . "/register/register.php");
    exit();
}

$page_content = template('catalog.php', []);
$title = 'Title';


print(template('layout.php', ['content' => $page_content, 'title' => $title]));